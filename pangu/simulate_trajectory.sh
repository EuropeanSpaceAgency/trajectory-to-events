#!/bin/bash
# Simulate trajectory with 3D Moon model

# Inputs
WDIR=$2
TRAJ="$WDIR/flight.fli"

# Outputs
VID_OUT="$WDIR/flight.mp4"

$PANGU_ROOT/bin/viewer \
	-ini ./pangu/pangu.ini \
	-shader_path $PANGU_ROOT/shaders \
    -flight $TRAJ \
    -movie_file $VID_OUT \
    -movie \
    -q \
    -width 1024 \
    -height 1024 \
    -fov 45 \
    -reflect hapke \
    -hapke_w 0.33 \
    -hapke_B0 0.95 \
    -hapke_h 0.05 \
    -hapke_L 0.05 \
    -hapke_scale 1.0 \
    -sun 1.496e11 155 2 \
    -colour 8 8 8 \
	-dynamic_shadows none \
    -dynamic_object 0 moon ignored  1 1 1  0 0 0 0.70710678 -0.70710678 0 0 1 \
    $PANGU_ROOT/models/lunar_world_dem/landing_$1_res/lunar_16_$1_res.pan
