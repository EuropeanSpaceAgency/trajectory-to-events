#!/bin/bash
# Generate static shadow maps (smap) for the 3D Lunar model

# Inputs
WDIR=$2

# Outputs
SMAP_OUT="$WDIR/lunar_16_$1_res_az=$4_el=$5_n=$6.smap"

$PANGU_ROOT/bin/mkshadows \
    -threads $3 \
    -sun 1.496e11 $4 $5 \
    -area \
    -samples $6 \
    -o $SMAP_OUT \
    $PANGU_ROOT/models/lunar_world_dem/landing_$1_res/lunar_16_$1_res.pan