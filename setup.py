from setuptools import setup
from setuptools import find_packages
from setuptools import find_namespace_packages


# --------------------------------------
from enum import IntEnum


class Version(IntEnum):
    major = 0
    minor = 0
    patch = 0


setup(
    name="traj2e",
    author="Loïc Azzalini | Emmanuel Blazquez | Alexander Hadjiivanov | Dario Izzo",
    version=f"{Version.major}.{Version.minor}.{Version.patch}",
    packages=find_namespace_packages(),
    install_requires=[
        "opencv-python==4.5.5.64",
        "sk-video==1.1.10",
        "fire==0.4.0",
        "numpy==1.23.4",
        "pandas",
        "scipy",
        "h5py",
        "jupyter",
        "jupyterlab",
        "matplotlib",
        "ipywidgets==7.6.5",
        "pytest",
        "command_runner",
        "numba==0.56.3",
        "profilehooks==1.12.0",
        "pytorch_lightning==1.8.6",
        "tqdm==4.63.0",
        "kornia==0.6.8",
        "amplpy",
        "seaborn",
        "torchvision"
    ],
    license="MIT",
    long_description=open("README.md").read(),
)
