import numpy as np
import os
import argparse
import glob
import subprocess
from tqdm import tqdm

from traj2e.trajectory import TrajectoryGenerator
from traj2e.events import VideoToEvents

def run_simulation():
    """
    Generate simulated landing videos from all the .fli flight files
    """

    # NOTE: in the future, we may want to gather all videos in a single folder
    # and use v2e_gpu with batch size > 1

    # Create video output directory (if it doesn't exist)
    # outdir = os.path.join("data", args.phase, "videos")
    # Path(outdir).mkdir(parents=True, exist_ok=True)
    
    # Glob flight files across landing profiles
    datafiles = glob.glob(os.path.join("data", args.phase, "**", "*.fli"), recursive=True)
    print("PANGU simulations - Generating {} flight video simulations".format(len(datafiles)))

    # Define bash command to run simulation
    bash_cmd = ["/bin/bash", "./pangu/simulate_trajectory.sh"]

    for file in tqdm(datafiles):
        # Extract working directory to pass to simulator script
        outdir = os.path.dirname(file)
        flight_filename = os.path.basename(file)
        # Append bash arguments
        # NOTE: replace "low" with "mid" for medium-quality 3D lunar model
        bash_args = bash_cmd + ["low", outdir]
        # Run PANGU script
        p = subprocess.run(bash_args)

def generate_shadow_maps(num_threads=10, num_samples=1):
    """
    Generate static shadow maps for each trajectory simulation

    Args:
        num_threads (int): number of threads to run in parallel
        num_samples (int): number of points to sample from Sun area. Defaults to 1 (point source)
    """

    # Glob flight files across landing profiles
    datafiles = glob.glob(os.path.join("data", args.phase, "**", "*.fli"), recursive=True)
    N = len(datafiles)
    print("Static shadow maps - Generating {} smaps".format(N))
    # Define bash command to run simulation
    bash_cmd = ["/bin/bash", "./pangu/generate_shadows.sh"]
    # TODO: Generate N Sun azimuth/elevation combinations
    azi_range = 156.55 * np.ones(N)
    ele_range = 1.35 * np.ones(N)

    for i in tqdm(range(N)):
        # Extract working directory to pass to simulator script
        outdir = os.path.dirname(datafiles[i])
        flight_filename = os.path.basename(datafiles[i])
        # Append bash arguments
        # NOTE: replace "low" with "mid" for medium-quality 3D lunar model
        bash_args = bash_cmd + ["low", outdir, num_threads, azi_range[i], ele_range[i], num_samples]
        # Run PANGU script
        p = subprocess.run(bash_args)

def generate_events():
    """
    Generate event-based representation of the simulated landing videos
    (using Metavision v2e_gpu toolkit)
    """

    # NOTE: in the future, we may want to gather all videos in a single folder
    # and use v2e_gpu with batch size > 1

    # Glob flight videos across landing profiles
    # video_path = os.path.join("data", args.phase, "videos")
    # events_path = os.path.join(video_path, "events.h5")
    # datafiles = glob.glob(os.path.join(video_path, "*.mp4"), recursive=True)

    datafiles = glob.glob(os.path.join("data", args.phase, "**", "*.mp4"), recursive=True)
    
    N = len(datafiles)
    print("Synthetic events  - Generating events from {} videos".format(N))

    # Create interface to v2e toolkit
    v2e = VideoToEvents(args.phase)

    # TODO: implement batch version of v2e_gpu
    for file in tqdm(datafiles):
        # Extract working directory to pass to simulator script
        outdir = os.path.dirname(file)
        v2e.run(outdir)

    # Save h5 file
    v2e.writer.close()

def parse_arguments():
    """
    Parse command line arguments
    """

    parser = argparse.ArgumentParser(prog="python eventful_dataset.py", formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument("-p", "--phase", type=str, choices=["braking", "approach", "descent"], default="approach", help="Lunar landing phase to simulate")
    parser.add_argument("-n", "--num_traj", type=int, default=1, help="Number of trajectories to generate")
    parser.add_argument("-v", "--viz", default=False, action="store_true", help="Save a snapshot of the optimal trajectory")
    parser.add_argument("-s", "--smap", default=False, action="store_true", help="Generate static shadow maps")
    args, usr_args = parser.parse_known_args()
    parser.print_help()

    return args, usr_args

if __name__ == "__main__":
    # Parse command line arguments
    args, usr_args = parse_arguments()

    # Lunar landing base profiles:
    if args.phase == "braking":
        profile = os.path.abspath("./traj2e/models/ocp/lunar_braking_profile.yml")
        ranges = os.path.abspath("./traj2e/models/ocp/lunar_braking_ranges.yml")
    elif args.phase == "descent":
        profile = os.path.abspath("./traj2e/models/ocp/lunar_descent_profile.yml")
        ranges = os.path.abspath("./traj2e/models/ocp/lunar_descent_ranges.yml")
    else:
        profile = os.path.abspath("./traj2e/models/ocp/lunar_approach_profile.yml")
        ranges = os.path.abspath("./traj2e/models/ocp/lunar_approach_ranges.yml")

    # Example usage of the traj2e module to create an event-based dataset:
    #   1. Initialize the trajectory generator with one of the base profiles
    generator = TrajectoryGenerator(parameters_path=profile)

    #   2. Start by solving the base OCP corresponding to the profile
    generator.solve_base_ocp(viz=args.viz)

    #   3. Generate n additional trajectories based on the profile parameter ranges
    generator.solve_n_ocp(n=args.num_traj, parameter_ranges_path=ranges, save_traj=True,
                          save_continuation=False, viz=args.viz)

    #   4. Generate the corresponding PANGU simulation videos
    run_simulation()

    #   5. Generate the corresponding event-based representations
    generate_events()
