import numpy as np
import cv2
from . import graphics
import time


# graphics
cam = graphics.Camera(
    pos=np.array([-5., 0., 0.]),
    theta=np.zeros(3),
    cameraMatrix=np.array([[1.e+3, 0., 768.], [0., 1.e+3, 432.], [0., 0., 1.]]),
    distCoeffs=np.array([0., 0., 0., 0., 0.])
)

cam.rotate([0., -0.5, 0.])

grid = graphics.create_grid(10, 10, 0.1)
big_grid = graphics.create_grid(3, 3, 1)

drone, forces = graphics.create_spacecraft(0.08)

ref = graphics.create_reference_system(0.1)

scl = 0.2
d = 0.8
b = 1

# options
#follow=False
#auto_play=False
#draw_path=False
#record=False

def nothing(x):
    pass
  
def animate(t, x, y, z, phi, theta, psi, u, file='output.mp4', multiple_trajectories=False, step=10, **kwargs):
#     global follow, auto_play, draw_path, draw_forces, record
    follow=True
    auto_play=False
    draw_path=True
    draw_forces=True  
    record=False
    
    traj_index = 0
    if multiple_trajectories:
        t_ = t[traj_index]
        pos = np.stack([x[traj_index],y[traj_index],z[traj_index]]).T
        ori = np.stack([phi[traj_index],theta[traj_index],psi[traj_index]]).T
        u_ = u[traj_index]
    else:
        t_ = t
        pos = np.stack([x,y,z]).T / 5000.
        ori = np.stack([phi,theta,psi]).T
        u_ = u
        
    cv2.namedWindow('animation')
    cv2.setMouseCallback('animation', cam.mouse_control)
    cv2.createTrackbar('t', 'animation', 0, t_.shape[0]-1, nothing)
        
    path = graphics.create_path([p for p in pos[0::5]])
    start_time = time.time()
    while True:
        if auto_play:
            current_time = time.time() - start_time
            time_time_index = -1
            for i in range(len(t_)):
                if t_[i] > current_time:
                    time_index = i - 1
                    break
            if time_index == -1:
                current_time = t_[time_index]
        else:
            time_index = cv2.getTrackbarPos('t', 'animation')
            current_time = t_[time_index]
 
        drone.translate(pos[time_index] - drone.pos)
        drone.rotate(ori[time_index])
  
        T = u_[time_index]                     # T = (T1, T2, T3, T4)
        graphics.set_thrust(drone, forces, T*scl)
   
        if follow:
            cam.set_center(drone.pos)
        else:
            cam.set_center(np.zeros(3))

        # using screen resolution of 1536x864
        frame = 255*np.ones((864, 1536, 3), dtype=np.uint8)
    
        # text
        cv2.putText(frame, "t = " + str(round(t_[time_index], 2)), (10, 20),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,0,0))

        # drawing
        big_grid.draw(frame, cam, color=(100, 100, 100), pt=1)
        grid.draw(frame, cam, color=(100, 100, 100), pt=1)
        ref.draw(frame, cam, color=(100, 100, 100), pt=5)

        if draw_path:
            path.draw(frame, cam, color=(0, 255, 0), pt=2)
        if pos[time_index][2] < 0:
            drone.draw(frame, cam, color=(255, 0, 0), pt=2)
        else:
            drone.draw(frame, cam, color=(0, 0, 255), pt=2)
        if draw_forces:
            for force in forces:
                force.draw(frame, cam, pt=2)
        if record:
            out.write(frame)
            cv2.putText(frame, '[recording]', (10, 40),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,0,0))

        control = cv2.waitKeyEx(1)
        if control == 106 and multiple_trajectories:      # J KEY
            time_index = 0
            start_time = time.time() - t_[time_index]
            traj_index = max(0, traj_index-step)
            t_ = t[traj_index]
            pos = np.stack([x[traj_index],y[traj_index],z[traj_index]]).T
            ori = np.stack([phi[traj_index],theta[traj_index],psi[traj_index]]).T
            u_ = u[traj_index]
            path = graphics.create_path([p for p in pos[0::5]])
        if control == 108 and multiple_trajectories:      # L KEY
            time_index = 0
            start_time = time.time() - t_[time_index]
            traj_index = min(len(t)-1, traj_index+step)
            t_ = t[traj_index]
            pos = np.stack([x[traj_index],y[traj_index],z[traj_index]]).T
            ori = np.stack([phi[traj_index],theta[traj_index],psi[traj_index]]).T
            u_ = u[traj_index]
            path = graphics.create_path([p for p in pos[0::5]])
        if control == 114:      # R KEY
            if record:             
                print('recording ended')
                out.release()
                print('recording saved in ' + file)
            else:  
                print('recording started')
                # videowriter
                fourcc = cv2.VideoWriter_fourcc(*'MP4V')
                out = cv2.VideoWriter(file, fourcc, 30., (1536, 864))
            record = not record
        if control == 102:      # F KEY
            follow = not follow
        if control == 112:      # P KEY
            draw_path = not draw_path 
        if control == 115:      # S KEY
            draw_forces = not draw_forces
        if control == 32:       # SPACE BAR
            auto_play = not auto_play
            start_time = time.time() - t_[time_index]
        if control == 49:       # 1
            cam.zoom(1.05)
        if control == 50:       # 2
            cam.zoom(1/1.05)
        if control == 27:       # ESCAPE
            break

        cv2.imshow('animation', frame)
    
    cv2.destroyAllWindows()

