import numpy as np
import seaborn as sns
import matplotlib
import matplotlib.pyplot as plt

sns.set(font_scale=2.0, rc={'text.usetex' : True, 'svg.fonttype': 'none'}, style="ticks")
matplotlib.rcParams['font.family'] = 'serif'
matplotlib.rcParams['font.serif'] = ['Computer Modern']

def plot_optimal_trajectory(time, states, inputs, title=[]):
    """
    Plot the OCP results in two figures:
        - fig. 1: 3D trajectory
        - fig. 2: states and inputs vs time

    Args:
        time (array, N): OCP time grid (i.e., control intervals)
        states (array, N x 13): optimal states
        inputs (array, N x 4): optimal control inputs
        title (list): list of titles
    
    Returns:
        fig1 (matplotlib figure): 3D trajectory
        fig2 (matplotlib figure): states and inputs vs time
    """

    # Plotting optimal trajectory
    fig1 = plt.figure()

    if len(title) >= 1:
        fig1.canvas.manager.set_window_title(title[0])

    ax1 = fig1.add_subplot(111, projection='3d')
    D= 3000
    ax1.set_xlim3d(-D, D)
    ax1.set_xlabel(r"$x [m]$")
    ax1.set_ylim3d(-D, D)
    ax1.set_ylabel(r"$y [m]$")
    ax1.set_zlim3d(0.0, D)
    ax1.set_zlabel(r"$z [m]$")
    ax1.plot(states[:, 0],states[:, 1],-states[:, 2])
    fig1.tight_layout()

    # Plotting optimal states and inputs
    fig2, ax2 = plt.subplots(nrows=3, ncols=2, figsize=(16, 16))

    if len(title) >= 2:
        fig2.canvas.manager.set_window_title(title[1])
    
    ax2[0, 0].plot(time, states[:, 0], label=r'$x$')
    ax2[0, 0].plot(time, states[:, 1], label=r'$y$')
    ax2[0, 0].plot(time, -states[:, 2], label=r'$z$')
    ax2[0, 0].set_ylabel(r'$\textrm{Position [m]}$')
    ax2[0, 0].legend(fontsize=14)

    ax2[0, 1].plot(time, states[:, 3], label=r'$v_x$')
    ax2[0, 1].plot(time, states[:, 4], label=r'$v_y$')
    ax2[0, 1].plot(time, -states[:, 5], label=r'$v_z$')
    ax2[0, 1].set_ylabel(r'$\textrm{Velocity [m/s]}$')
    ax2[0, 1].legend(fontsize=14)

    ax2[1, 0].plot(time, states[:, 6], label=r'$\phi$')
    ax2[1, 0].plot(time, states[:, 7], label=r'$\theta$')
    ax2[1, 0].plot(time, states[:, 8], label=r'$\psi$')
    ax2[1, 0].set_ylabel(r'$\textrm{Attitude [rad]}$')
    ax2[1, 0].legend(fontsize=14)

    ax2[1, 1].plot(time, states[:, 9],  label=r'$p$')
    ax2[1, 1].plot(time, states[:, 10], label=r'$q$')
    ax2[1, 1].plot(time, states[:, 11], label=r'$r$')
    ax2[1, 1].set_ylabel(r'$\textrm{Attitude rates [rad/s]}$')
    ax2[1, 1].legend(fontsize=14)

    ax2[2, 0].plot(time, states[:, 12],  label=r'$m$')
    ax2[2, 0].legend(fontsize=14)
    ax2[2, 0].set_xlabel(r'$\textrm{Time t [s]}$')
    ax2[2, 0].set_ylabel(r'$\textrm{Mass [kg]}$')

    ax2[2, 1].plot(time, inputs[:, 0], 'k-', label=r'$u_T$')
    ax2[2, 1].plot(time, inputs[:, 1], 'k--', label=r'$u_{\phi}$')
    ax2[2, 1].plot(time, inputs[:, 2], 'k-.', label=r'$u_{\theta}$')
    ax2[2, 1].plot(time, inputs[:, 3], 'k:', label=r'$u_{\psi}$')
    ax2[2, 1].set_xlabel(r'$\textrm{Time t [s]}$')
    ax2[2, 1].set_ylabel(r'$\textrm{Normalized thrust}$')
    ax2[2, 1].legend(fontsize=14)

    fig2.tight_layout()

    return fig1, fig2

# TODO: reverse flow_uv -> colorwheel mapping
def make_colorwheel():
    """
    Generates a color wheel for optical flow visualization as presented in:
        Baker et al. "A Database and Evaluation Methodology for Optical Flow" (ICCV, 2007)
        URL: http://vision.middlebury.edu/flow/flowEval-iccv07.pdf

    Code follows the original C++ source code of Daniel Scharstein.
    Code follows the the Matlab source code of Deqing Sun.

    Returns:
        np.ndarray: Color wheel
    """

    RY = 15
    YG = 6
    GC = 4
    CB = 11
    BM = 13
    MR = 6

    ncols = RY + YG + GC + CB + BM + MR
    colorwheel = np.zeros((ncols, 3))
    col = 0

    # RY
    colorwheel[0:RY, 0] = 255
    colorwheel[0:RY, 1] = np.floor(255*np.arange(0,RY)/RY)
    col = col+RY
    # YG
    colorwheel[col:col+YG, 0] = 255 - np.floor(255*np.arange(0,YG)/YG)
    colorwheel[col:col+YG, 1] = 255
    col = col+YG
    # GC
    colorwheel[col:col+GC, 1] = 255
    colorwheel[col:col+GC, 2] = np.floor(255*np.arange(0,GC)/GC)
    col = col+GC
    # CB
    colorwheel[col:col+CB, 1] = 255 - np.floor(255*np.arange(CB)/CB)
    colorwheel[col:col+CB, 2] = 255
    col = col+CB
    # BM
    colorwheel[col:col+BM, 2] = 255
    colorwheel[col:col+BM, 0] = np.floor(255*np.arange(0,BM)/BM)
    col = col+BM
    # MR
    colorwheel[col:col+MR, 2] = 255 - np.floor(255*np.arange(MR)/MR)
    colorwheel[col:col+MR, 0] = 255
    return colorwheel

def flow_uv_to_colors(u, v, convert_to_bgr=False):
    """
    Applies the flow color wheel to (possibly clipped) flow components u and v.

    According to the C++ source code of Daniel Scharstein
    According to the Matlab source code of Deqing Sun

    Args:
        u (np.ndarray): Input horizontal flow of shape [H,W]
        v (np.ndarray): Input vertical flow of shape [H,W]
        convert_to_bgr (bool, optional): Convert output image to BGR. Defaults to False.

    Returns:
        np.ndarray: Flow visualization image of shape [H,W,3]
    """
    flow_image = np.zeros((u.shape[0], u.shape[1], 3), np.uint8)
    colorwheel = make_colorwheel()  # shape [55x3]
    ncols = colorwheel.shape[0]
    rad = np.sqrt(np.square(u) + np.square(v))
    a = np.arctan2(v, -u)/np.pi
    fk = (a+1) / 2*(ncols-1)
    k0 = np.floor(fk).astype(np.int32)
    k1 = k0 + 1
    k1[k1 == ncols] = 0
    f = fk - k0
    for i in range(colorwheel.shape[1]):
        tmp = colorwheel[:,i]
        col0 = tmp[k0] / 255.0
        col1 = tmp[k1] / 255.0
        col = (1-f)*col0 + f*col1
        idx = (rad <= 1)
        col[idx]  = 1 - rad[idx] * (1-col[idx])
        col[~idx] = col[~idx] * 0.75   # out of range
        # Note the 2-i => BGR instead of RGB
        ch_idx = 2-i if convert_to_bgr else i
        flow_image[:,:,ch_idx] = np.floor(255 * col)
    return flow_image

def flow_to_color(u, v, clip_flow=None, convert_to_bgr=False):
    """
    Expects a two dimensional flow image of shape.

    Args:
        flow_uv (np.ndarray): Flow UV image of shape [H,W,2]
        clip_flow (float, optional): Clip maximum of flow values. Defaults to None.
        convert_to_bgr (bool, optional): Convert output image to BGR. Defaults to False.

    Returns:
        np.ndarray: Flow visualization image of shape [H,W,3]
    """
    # assert flow_uv.ndim == 3, 'input flow must have three dimensions'
    # assert flow_uv.shape[2] == 2, 'input flow must have shape [H,W,2]'
    # if clip_flow is not None:
    #     flow_uv = np.clip(flow_uv, 0, clip_flow)
    # u = flow_uv[:,:,0]
    # v = flow_uv[:,:,1]
    rad = np.sqrt(np.square(u) + np.square(v))
    rad_max = np.max(rad)
    epsilon = 1e-5
    u = u / (rad_max + epsilon)
    v = v / (rad_max + epsilon)
    return flow_uv_to_colors(u, v, convert_to_bgr)