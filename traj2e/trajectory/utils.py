import numpy as np

def rotation123(phi, theta, psi):
    cph = np.cos(phi)
    sph = np.sin(phi)
    cth = np.cos(theta)
    sth = np.sin(theta)
    cps = np.cos(psi)
    sps = np.sin(psi)
    R_x = np.array([[1, 0, 0],
                    [0, cph, -sph],
                    [0, sph, cph]
                    ])
    R_y = np.array([[cth, 0, sth],
                    [0, 1, 0],
                    [-sth, 0, cth]
                    ])
    R_z = np.array([[cps, -sps, 0],
                    [sps, cps, 0],
                    [0, 0, 1]
                    ])
    R = np.dot(R_z, np.dot(R_y, R_x))
    return R

def motion_field(sx, sy, state, fx=1, fy=1, target_radius=None):
    """
    Computes the motion field assuming a planar or spherical target body

    Args:
        sx (float or np.array): x position in the sensor
        sy (float or np.array): y position in the sensor
        state (np.array): the spacecraft state x,y,z,vx,vy,vz,phi,theta,psi,p,q,r
        fx (float): horizontal focal length. Defaults to 1
        fy (float): vertical focal length. Defaults to 1
        target_radius (float): radius of target in m. Defaults to None (planar case)

    Returns:
        u, v: the motion field
    """
    x,y,z,vx,vy,vz,phi,theta,psi,p,q,r = state
    h = np.zeros_like(sx)
    u = np.zeros_like(sx)
    v = np.zeros_like(sx)
    quad_sol = np.zeros_like(sx)

    # Transform inertial to camera frame
    R = rotation123(phi, theta,psi)
    vxc, vyc,vzc = R.transpose()@[[vx],[vy],[vz]]
    alpha, beta, gamma = R[2,:]

    if target_radius is not None: # landing on spherical surface
        # TODO: verify quadratic solution
        H = -z
        b = -2 * (target_radius + H) * (alpha * sx / fx + beta * sy/fy + gamma)
        a = (sx/fx)**2 + (sy/fy)**2 + 1
        c = 2 * target_radius * H + H**2
        fac = 4 * a * c
        delta = b**2 - fac
        # delta = (target_radius - z)**2 * (alpha * sx/fx + beta * sy/fy + gamma)**2 + 2 * target_radius * z * ((sx/fx)**2 + (sy/fy)**2 + 1)
        # Solve for positive root of quadratic equation
        pos = delta >= 0
        quad_sol[pos] = - b[pos] / (2 * a[pos]) - np.sqrt(delta[pos]) / (2 * a[pos])
        # quad_sol[pos] = ((target_radius - z) * (alpha * sx[pos]/fx + beta * sy[pos]/fy + gamma)) / ((sx[pos]/fx)**2 + (sy[pos]/fy)**2 + 1) - \
        #     np.sqrt(delta[pos]) / ((sx[pos]/fx)**2 + (sy[pos]/fy)**2 + 1)
        h[pos] = 1 / quad_sol[pos]
        mask = quad_sol >= 0
        # mask = pos

        # Motion field equations
        u[mask] = (sx[mask]*vzc - fx*vxc) * h[mask] - fx * q + r * sy[mask] + p * sx[mask] * sy[mask] / fy - q * sx[mask]**2 / fx
        v[mask] = (sy[mask]*vzc - fy*vyc) * h[mask] + fy * p - r * sx[mask] - q * sx[mask] * sy[mask] / fx + p * sy[mask]**2 / fy
        # u = (sx*vzc - fx*vxc) * h - fx * q + r * sy + p * sx * sy / fy - q * sx**2 / fx
        # v = (sy*vzc - fy*vyc) * h + fy * p - r * sx - q * sx * sy / fx + p * sy**2 / fy

    else: # landing on planar surface
        h = - (alpha*sx/fx + beta*sy/fy + gamma) / z
        # Motion field equations
        u = (sx*vzc - fx*vxc) * h - fx * q + r * sy + p * sx * sy / fy - q * sx**2 / fx
        v = (sy*vzc - fy*vyc) * h + fy * p - r * sx - q * sx * sy / fx + p * sy**2 / fy

    return u,v