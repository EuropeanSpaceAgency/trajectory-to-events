import numpy as np
from scipy.interpolate import interp1d
from scipy.spatial.transform import Rotation
from pathlib import Path
from datetime import datetime
from time import perf_counter
import yaml
import copy
import os

# Importing modules
from traj2e.trajectory.ampl_interface import AMPLModel
from traj2e.viz.plots import plot_optimal_trajectory


class TrajectoryGenerator:

    def __init__(self,
                 parameters_path="./traj2e/models/ocp/lunar_approach_profile.yml",
                 output_path="./data",
                 landing_model_path="./traj2e/models/ampl/spacecraft_12dof_moon.mod"
                ):
        """
        
        """

        # Setup OCP solver
        self.ampl_model = AMPLModel(landing_model_path, auto_reset=False)
        self.output_path = os.path.join(output_path)

        # Determine initial set of parameters from user config
        self.profile = os.path.split(parameters_path)[-1].split("_")[1]
        self.base_params, self.continuation_params = self.initialize_ocp_parameters(parameters_path)
        # Initialize AMPL model
        self.ampl_model[self.base_params] = self.base_params

    def initialize_ocp_parameters(self, parameters_path):
        """
        (Re)initialize the OCP parameters based on specified trajectory parameters
        
        Args:
            parameters_path (string): path to parameter file
        """

        # Load OCP parameters
        with open(parameters_path, "r") as fid:
            try:
                self.traj_config = yaml.safe_load(fid)
                self.params = copy.deepcopy(self.traj_config["parameters"])
            except yaml.YAMLError as exc:
                print(exc)

        params, param_ranges = self.extract_ocp_params(self.params)

        # Relax the problem if needed
        if "relax" in self.traj_config.keys():
            for param in self.traj_config["relax"]:
                constraint = self.ampl_model.getConstraint(param)
                constraint.drop()
        return params, param_ranges

    def extract_ocp_params(self, p):
        """
        Extract user-defined parameters for the OCP
        """

        # Base parameters
        starter_params = dict.fromkeys(p)
        # List of parameters that require continuation
        continuation = []

        for k, v in p.items():
            if isinstance(v, list):
                # Add key to list of parameters requiring continuation
                continuation.append(k)
                # Add first value to starter parameters
                starter_params[k] = p[k].pop(0)
            else:
                starter_params[k] = p[k]
        return starter_params, continuation
    
    def sample_ocp_params(self, parameter):
        """
        Sample specified trajectory parameter range to generate new OCP

        Args:
            parameter:

        Returns:
        """
        sampling_range = np.arange(*self.traj_config["parameters"][parameter])
        new_parameter = np.random.choice(sampling_range)

        return new_parameter

    def solve_n_ocp(self, n, parameter_ranges_path, hide_solver_output=True, viz=False, save_traj=True, save_continuation=False):
        """
        Solve n extended OCP according to specified trajectory parameter ranges

        Args:
            n (integer): the number of trajectories to generate
            parameter_ranges_path (string): path to parameter ranges file
            hide_solver_output (bool):
            viz (bool): save snapshot of trajectory in output directory
            save_traj (bool): whether the trajectory states and flight file should be saved
            save_continuation (bool): whether intermediary continuation trajectories should be saved        
        """

        # TODO: this next part could be more elegantly coded
        params, param_ranges = self.initialize_ocp_parameters(parameter_ranges_path)
        self.ampl_model[params] = self.base_params

        # Solve n extended OCPs
        for i in range(n):
            
            # Use continuation to solve for one new parameter at a time
            for p in param_ranges:
                # Vary OCP from base profile
                old_p = self.ampl_model[p]
                new_p = self.sample_ocp_params(p)
                # Update AMPL OCP model
                self.ampl_model[p] = new_p
                print("#{} OCP - Solving updated trajectory: {}={}({})".format(i+1, p, new_p, old_p))
                t0 = perf_counter()
                _success = self.ampl_model.solve()
                t1 = perf_counter()

                if not _success:
                    print("FAIL")
                    break

                print("#{} OCP - Success: {}".format(i+1, _success))
                print("#{} OCP - Final time: {} sec.".format(i+1, self.ampl_model['tf']))
                print("#{} OCP - Final mass: {} kg".format(i+1, self.ampl_model['m'][-1]))
                print("#{} OCP - Wall time: {} s".format(i+1, t1 - t0))
                print(50 * "=")

                if save_continuation:
                    self.save_trajectory(viz=viz)

            if save_traj and not save_continuation:
                self.save_trajectory(viz=viz)
    
    def solve_base_ocp(self, save_traj=True, hide_solver_output=True, viz=False):
        """
        Solve the base OCP according to specified trajectory parameters

        Args:
            save_traj (bool): whether the base trajectory states and flight file should be saved
            hide_solver_output (bool):
            viz (bool):
        """
        
         # Solving base OCP
        print("Base OCP - Solving base trajectory")
        t0 = perf_counter()
        _success = self.ampl_model.solve()
        t1 = perf_counter()

        print("Base OCP - Success: ", _success)
        print("Base OCP - Final time: ", self.ampl_model['tf'], "sec.")
        print("Base OCP - Final mass: ", self.ampl_model['m'][-1], "kg")
        print("Base OCP - Wall time: ", t1 - t0, "s")
        print(50 * "=")

        # Perform continuation
        for param in self.continuation_params:
            while self.params[param]:
                self.ampl_model[param] = self.params[param].pop(0)

                # Solving revised OCP from previous solution
                t0 = perf_counter()
                _success = self.ampl_model.solve()
                t1 = perf_counter()

                print("Base OCP - Success: ", _success)
                print("Base OCP - Final time: ", self.ampl_model['tf'], "sec.")
                print("Base OCP - Final mass: ", self.ampl_model['m'][-1], "kg")
                print("Base OCP - Wall time: ", t1 - t0, "s")
                print(50 * "=")
        
        # Save base parameters
        self.base_params = self.ampl_model.getParameterValues()

        if save_traj:
            outdir = self.save_trajectory(viz=viz)
        
        return outdir

    def save_trajectory(self, filename=None, viz=False):
        """
        
        """

        # Extracting most-recent trajectory
        t = self.ampl_model['t']
        x = np.vstack(
            [*self.ampl_model['x','y','z','vx','vy','vz','phi','theta', 'psi', 'p','q','r', 'm'].values()]).T
        u = self.ampl_model['u']

        # Saving results
        date_time = datetime.now().isoformat()
        outdir = os.path.join(self.output_path, self.profile, date_time)
        Path(outdir).mkdir(parents=True, exist_ok=True)

        # Save the trajectory states file
        traj_filename = "traj.npy" if filename is None else filename + ".npy"
        traj_outfile = os.path.join(outdir, traj_filename)
        np.save(traj_outfile, {"t": t, "x": x, "u": u})
        print("Save - Trajectory written to: {}".format(traj_outfile))

        # Save snapshot of trajectory
        if viz:
            traj_snapshot = os.path.join(outdir, "traj.jpeg")
            _, fig = plot_optimal_trajectory(t, x, u)
            fig.savefig(traj_snapshot)
            del fig

        # Save a copy of the trajectory parameters
        traj_param_filename = "parameters.yml" if filename is None else filename + ".yml"
        traj_param_outfile = os.path.join(outdir, traj_param_filename)
        with open(traj_param_outfile, "w") as fid:
            yaml.dump(self.ampl_model.getParameterValues(), fid)
        print("Save - Trajectory parameters written to: {}".format(traj_param_outfile))

        # Save the PANGU flight file
        pangu_flight = self.generate_flight_file(t, x)
        flight_filename = "flight.fli" if filename is None else filename + ".fli"
        flight_outfile = os.path.join(outdir, flight_filename)
        with open(flight_outfile, "w") as fid:
            fid.writelines(pangu_flight)
        print("Save - PANGU flight file ({} frames) written to: {}".format(len(pangu_flight)-1, flight_outfile))

        return outdir

    def generate_flight_file(self, t, x, fps=100):
        """
        Generate PANGU flight file based on optimal trajectory interpolation
        """

        # Interpolating the optimal descent
        interpolate = interp1d(t, x, kind='cubic', axis=0)
        # Computing the number of frames to generate
        # TODO: read in frames per second from a spec file
        fps = 30
        n_frames = int(t[-1] * fps)
        # Computing the time grid and iterpolating the state
        grid = np.linspace(0,t[-1], n_frames)
        new_states = interpolate(grid)

        # We translate the problem so that the trajectory ends at the landing site
        # TODO: make landing site class member variable
        x_off, y_off, z_off = [-198974, 49, 1730162]

        # Content of the PANGU flight (.fli) file
        content = ["view craft\n"]
        for state in new_states:
            # Euler angle to quaternion conversion
            R = Rotation.from_euler(seq="ZYX", angles=[-np.pi-state[8], -state[7], np.pi+state[6]], degrees=False)
            quat = R.as_quat()
            content.append("quaternion {} {} {} {} {} {} {}\n".format(-(state[0]-x_off), state[1]-y_off, -state[2]+z_off, quat[-1], quat[0], quat[1], quat[2]))
            # Original with Euler angles
            # content.append("start {} {} {} {} {} {}\n".format(state[0]-x_off, -(state[1]-y_off), -state[2], 270-state[8]/np.pi*180, -90+state[7]/np.pi*180, 180+state[6]/np.pi*180))
        return content