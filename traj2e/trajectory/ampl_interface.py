import io
import contextlib
from amplpy import AMPL
import numpy as np

class AMPLModel:
    def __init__(self, ampl_mod_path, auto_reset=True):
        self.ampl = AMPL()
        self.ampl.read(ampl_mod_path)
        self.ampl_mod_path = ampl_mod_path
        
        self.__parameters_that_have_been_set = dict()
        self.__pristine = True
        self.__ampl_output = None
        self.auto_reset = auto_reset
        self.reset()
        
    def getObjectives(self):
        return self.ampl.getObjectives()
        
    def getObjective(self, name):
        return self.ampl.getObjective(name)

    def getConstraints(self):
        return self.ampl.getConstraints()
        
    def getConstraint(self, name):
        return self.ampl.getConstraint(name)
        
    def getVariables(self):
        return self.ampl.getVariables()
        
    def getVariable(self, name):
        return self.ampl.getVariable(name)
    
    def getParameters(self):
        return self.ampl.getParameters()
    
    def getParameter(self, name):
        return self.ampl.getParameter(name)
    
    def getAMPL(self):
        return self.ampl
    
    def display(self, *args):
        return self.ampl.display(*args)
    
    def reset(self):
        if not self.__pristine or True:
            self.ampl.reset()
            self.ampl.read(self.ampl_mod_path)
            self.parameter_names = {i[0]:i[1] for i in self.ampl.getParameters()}
            self.variable_names  = {i[0]:i[1] for i in self.ampl.getVariables()}

            if "timegrid" in self.variable_names:
                self.variable_names = {"t": self.variable_names['timegrid'], **self.variable_names}
            self.objective_names = {i[0]:i[1] for i in self.ampl.getObjectives()}

            self.special_variables = {key: (key, key+"m") for key in self.variable_names if key+"m" in self.variable_names}
            self._special_vars_key_set = {val[0] for _,val in self.special_variables.items()} | {val[1] for _,val in self.special_variables.items()}

            self._keys = list(self.keys())
            self.solved = False
            self.__pristine = True
            self.__updateUnderlyingValues()
    
    def checkSolved(self):
        if not self.solved:
            ampl_out = io.StringIO()
            with contextlib.redirect_stdout(ampl_out):
                self.ampl.display("solve_result")
            result_status = ampl_out.getvalue().rstrip()
            if result_status == "solve_result = solved":
                self.solved = True
            else:
                self.solved = False
            self.__pristine = False
        return self.solved
    
    def solve(self, hide_solver_output=True):
        try:
            if not self.checkSolved() or not self.auto_reset:
                self.__ampl_output = io.StringIO()
                if hide_solver_output:
                    with contextlib.redirect_stdout(self.__ampl_output):
                        self.ampl.solve()
                else:
                    self.ampl.solve()
            return self.checkSolved()
        except KeyboardInterrupt:
            return self.checkSolved()
        except:
            raise
    
    def solveAsync(self, callback, hide_solver_output=True):
        if not self.checkSolved():
            self.__ampl_output = io.StringIO()
            if hide_solver_output:
                with contextlib.redirect_stdout(self.__ampl_output):
                    return self.ampl.solveAsync(callback)
            else:
                return self.ampl.solveAsync(callback)
            
        
    def showAMPLOutput(self):
        return self.__ampl_output.getvalue().rstrip()
        
    def getSolutionValues(self, name_list=None):
        if not self.solved:
            self.solve()
        if name_list is not None:
            if isinstance(name_list, str):
                name_list = [name_list]
            name_list = {i for i in name_list if i in self.variable_names}
        else:
            name_list = set(self.variable_names.keys())
        name_is_special = name_list & set(self.special_variables.keys())
        return self[name_list]
        
    def getVariableValues(self, name_list=None):
        if name_list is not None:
            if isinstance(name_list, str):
                name_list = [name_list]
            name_list = {i for i in name_list if i in self.variable_names}
        else:
            name_list = set(self.variable_names.keys())
        name_is_special = name_list & set(self.special_variables.keys())
        return {
            **self.getSpecialVariable(name_is_special),
            **{key: self.getVariableValue(key) for key in name_list - name_is_special}
        }
        
    def getParameterValues(self, name_list=None):
        if name_list is not None:
            if isinstance(name_list, str):
                name_list = [name_list]
            return {key: self.getParameterValue(key) for key in name_list if key in self.parameter_names}
        else:
            return {key: self.getParameterValue(key) for key in self.parameter_names}
        
    def getObjectiveValues(self, name_list=None):
        if name_list is not None:
            if isinstance(name_list, str):
                name_list = [name_list]
            return {key: self.getObjectiveValue(key) for key in name_list if key in self.objective_names}
        else:
            return {key: self.getObjectiveValue(key) for key in self.objective_names}
        
    def getVariableValue(self, var_name):
        if self.variable_names[var_name].indexarity() == 0:
            return self.variable_names[var_name].getValues().toList()[0]
        else:
            # temp = np.array(self.variable_names[var_name].getValues().toList())
            # temp = temp[:, self.variable_names[var_name].indexarity():]
            temp = np.array([instance.value() for index, instance in self.variable_names[var_name].instances()])
            return temp.reshape(self.getVarShape(var_name))
        
    def getParameterValue(self, param_name):
        if self.parameter_names[param_name].indexarity() == 0:
            return self.parameter_names[param_name].getValues().toList()[0]
        else:
            # temp = np.array(self.parameter_names[param_name].getValues().toList())
            # temp = temp[:, self.parameter_names[param_name].indexarity():]
            temp = np.array([value for index, value in self.parameter_names[param_name].instances()])
            return temp.reshape(self.getParamShape(param_name))
    
    def getObjectiveValue(self, obj_name):
        return np.array(self.objective_names[obj_name].getValues().toList()).reshape(self.getObjectiveShape(obj_name))
        
    def getSpecialVariable(self, name_list=None):
        return_dict = dict()
        if name_list is not None:
            if isinstance(name_list, str):
                if name_list in self.special_variables:
                    return self.coerceSpecialVariable(name_list)
            for i in name_list:
                if i in self.special_variables:
                    return_dict.update({i: self.coerceSpecialVariable(i)})
        else:
            for i in self.special_variables:
                return_dict.update({i: self.coerceSpecialVariable(i)})
        return return_dict
    
    def coerceSpecialVariable(self, var_name):
        endpoint_vals = self.getVariableValue(self.special_variables[var_name][0])
        midpoint_vals = self.getVariableValue(self.special_variables[var_name][1])
        assert(endpoint_vals.shape[1:] == midpoint_vals.shape[1:])
        final_shape_tuple = list(endpoint_vals.shape[1:])
        temp = np.empty((endpoint_vals.shape[0] + midpoint_vals.shape[0], *final_shape_tuple), dtype=endpoint_vals.dtype)
        temp[::2] = endpoint_vals
        temp[1::2] = midpoint_vals
        return temp
    
    def getVarShape(self, var_name):
        ret_tuple = []
        if self.getVariable(var_name).indexarity() == 0:
            return tuple()
        else:
            for i in self.getVariable(var_name).getIndexingSets():
                ret_tuple.append(self.getSetSize(i.split("in")[-1].strip()))
        return tuple(ret_tuple)
    
    def getParamShape(self, param_name):
        ret_tuple = []
        if self.getParameter(param_name).indexarity() == 0:
            return tuple()
        else:
            for i in self.getParameter(param_name).getIndexingSets():
                ret_tuple.append(self.getSetSize(i.split("in")[-1].strip()))
        return tuple(ret_tuple)
    
    def getObjectiveShape(self, obj_name):
        ret_tuple = []
        if self.getObjective(obj_name).indexarity() == 0:
            return tuple()
        else:
            for i in self.getObjective(obj_name).getIndexingSets():
                ret_tuple.append(self.getSetSize(i.split("in")[-1].strip()))
        return tuple(ret_tuple)
    
    def getSetSize(self, set_name):
        return len(self.ampl.getSet(set_name).getValues().toList())
    
    def setParameterValue(self, param_name, param_value):
        if param_name in self.parameter_names:
            if self.auto_reset:
                if param_name not in self.__parameters_that_have_been_set:
                    self.reset()
                elif not np.all(np.isclose(self.__parameters_that_have_been_set[param_name], param_value, rtol=1e-10, atol=1e-12)):
                    self.reset()
            self.__parameters_that_have_been_set[param_name] = param_value
        return self.__updateUnderlyingValues()
            
    def setParameterValues(self, param_dict=dict(), **kwargs):
        for key,val in {**param_dict, **kwargs}.items():
            self.setParameterValue(key, val)
        return self
    
    def __updateUnderlyingValue(self, param_name, param_value):
        if param_name in self.parameter_names:
            self.ampl.param[param_name] = param_value
        return self
    
    def __updateUnderlyingValues(self):
        for key, val in self.__parameters_that_have_been_set.items():
            self.__updateUnderlyingValue(key, val)
        return self
            
    def __getitem__(self, key):
        if isinstance(key, int):
            return self._keys[key], self[self._keys[key]]
        elif isinstance(key, str):
            if key in self.parameter_names:
                return self.getParameterValue(key)
            elif key == "timegrid" or key == "t":
                num_nodes = int(self['n'])
                x_vals = np.empty((num_nodes*2 - 1,), dtype=np.float64)
                x_vals[::2] = self.getVariableValue("timegrid")
                x_vals[1::2] = self.getVariableValue("timegrid")[:-1] + self.getVariableValue("dt") / 2.0
                return x_vals
            elif key in self.special_variables:
                return self.getSpecialVariable(key)
            elif key in self.variable_names:
                return self.getVariableValue(key)
            elif key in self.objective_names:
                return self.getObjectiveValue(key)
            else:
                raise KeyError("key {} not in model".format(key))
        else:
            return {_key: self[_key] for _key in key}
            
    def __setitem__(self, key, val):
        if isinstance(key, str):
            if key in self.parameter_names:
                return self.setParameterValue(key, val)
            else:
                raise KeyError("Key not in parameters!")
        else:
            if isinstance(val, dict):
                if (set(key) & set(val.keys())) != set(key):
                    raise ValueError("Specified keys not in dict of values to assign.")
                for _key in key:
                    self[_key] = val[_key]
            else:
                if len(key) != len(val):
                    raise ValueError("Number of parameters does not match number of values. Received {} values, expected {}.".format(len(val), len(key)))
                for _key,_val in zip(key,val):
                    self[_key] = _val
                
    def items(self):
        return ((key, self[key]) for key in self.keys())
    
    def keys(self):
        return sorted(self.parameter_names.keys()) + \
               sorted(self.special_variables.keys()) + \
               sorted(set(self.variable_names.keys()) - self._special_vars_key_set) + \
               sorted(self.objective_names.keys())
            
    def __iter__(self):
        return (key for key in self.keys())
        
    def __len__(self):
        return len(self.keys())
    
