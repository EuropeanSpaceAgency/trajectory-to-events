import numpy as np
import torch
import cv2
import h5py
import yaml
import os

from torchvision.utils import make_grid
from metavision_core_ml.utils.torch_ops import normalize_tiles
from metavision_core_ml.video_to_event.video_stream_dataset import make_video_dataset
from metavision_core_ml.video_to_event.gpu_simulator import GPUEventSimulator
from metavision_core_ml.preprocessing.event_to_tensor_torch import event_image

class VideoToEvents:

    def __init__(self, phase):
        """
        Initialize parameters of the video-to-event converter (Metavision)
        """
        
        # Create h5 dataset writer
        self.writer = h5py.File(os.path.join("data", phase, "events.h5"), "w")
        # Read in v2e parameters
        with open("./traj2e/events/v2e_parameters.yml", "r") as fid:
            try:
                self.params = yaml.safe_load(fid)
            except yaml.YAMLError as exc:
                print(exc)
    
    def run(self, path):
        """
        Run the video-to-event utility after updating v2e parameters

        Args:
            path (string): path to video file
        """
    
        # Update path of video
        self.params["path"] = path
        self.v2e(**self.params)

    def v2e(self, path, threshold_mu, threshold_std, refractory_period, leak_rate_hz, cutoff_hz,
            shot_noise_hz, height, width, num_workers=8, batch_size=1, max_frames_per_video=5000,
            device="cuda:0", mode="", split_channels=False, min_frames_per_batch=2, max_frames_per_batch=10,
            nbins=10, save_video=True, save_events=True, random_noise=False):
        """
        Video-to-event generation based on:
        https://github.com/prophesee-ai/openeb/blob/main/sdk/modules/core_ml/python/samples/viz_video_to_event_gpu_simulator/
        """

        nrows = 2 ** ((batch_size.bit_length() - 1) // 2)
        dl = make_video_dataset(
            path, num_workers, batch_size, height, width, max_frames_per_video - 1, max_frames_per_video,
            min_frames=min_frames_per_batch, max_frames=max_frames_per_batch, rgb=False)
        event_gpu = GPUEventSimulator(batch_size, height, width, threshold_mu,
                                    threshold_std, refractory_period, leak_rate_hz, cutoff_hz, shot_noise_hz)
        
        event_gpu.to(device)
        dl.to(device)
        pause = False
        last_images = None

        if save_video:
            # TODO: accumulate event-frames based on dt to recover original fps
            video_path = os.path.join(path, "events.mp4")
            out = cv2.VideoWriter(video_path, cv2.VideoWriter_fourcc('m','p','4','v'), 30, (width, height))
        if save_events:
            dataset = self.writer.create_dataset(
                name=os.path.basename(path),
                shape=(0, 4), #(x, y, p, t)
                maxshape=(None, 4),
                dtype="uint32",
                compression="gzip"
            )

        for i, batch in enumerate(dl):
            images = batch['images'].squeeze(dim=0)

            first_times = batch['first_times']
            timestamps = batch['timestamps']
            num_frames = batch['video_len']

            if pause and last_images is not None:
                images = last_images

            # Randomize event-camera emulation parameters
            if random_noise:
                event_gpu.randomize_broken_pixels(first_times, video_proba=0.1)
                event_gpu.randomize_thresholds(first_times)
                event_gpu.randomize_cutoff(first_times)
                event_gpu.randomize_shot(first_times)
                event_gpu.randomize_refractory_periods(first_times)

            log_images = event_gpu.dynamic_moving_average(images, num_frames, timestamps, first_times)

            if mode == 'counts':
                idx = 1
                counts = event_gpu.count_events(log_images, num_frames, timestamps, first_times)
            elif mode == 'event_volume':
                voxels = event_gpu.event_volume(log_images, num_frames, timestamps,
                                                first_times, nbins, mode, split_channels=split_channels)
                if split_channels:
                    counts = voxels[:, nbins:] - voxels[:, :nbins]
                    counts = counts.mean(dim=1)
                else:
                    counts = voxels.mean(dim=1)
            else:
                events = event_gpu.get_events(log_images, num_frames, timestamps, first_times)
                # print(events.detach().cpu().numpy().shape)
                counts = event_image(events, batch_size, height, width)

            im = 255 * normalize_tiles(counts.unsqueeze(1).float(), num_stds=3)

            im = make_grid(im, nrow=nrows).detach().cpu().permute(1, 2, 0).numpy().astype(np.uint8)

            blur_images = torch.exp(log_images) * 255
            first_frames_indices = torch.cat((torch.zeros(1, device=images.device), num_frames.cumsum(0)))[
                :-1].long()
            imin = make_grid(
                blur_images[None, ..., first_frames_indices].permute(3, 0, 1, 2), nrow=nrows).detach().cpu().permute(
                1, 2, 0).numpy().astype(np.uint8)
            final = np.concatenate((im, imin), axis=1)

            if save_video:
                out.write(im)

            if save_events:
                temp_events = events.detach().cpu().numpy()
                srt_idx = np.argsort(temp_events[:, -1])
                dataset.resize(dataset.shape[0] + temp_events.shape[0], axis=0)
                # TODO: make use of batch number when bs neq 1
                dataset[-temp_events.shape[0]:] = temp_events[srt_idx][:, 1:] # disregards batch number since bs=1

            last_images = images

        if save_video:
            out.release()