# traj2e: Trajectory to Events

This repository contains the trajectory-to-event pipeline detailed in:

> Loïc J. Azzalini, Emmanuel Blazquez, Alexander Hadjiivanov, Gabriele Meoni, and Dario Izzo.
On the Generation of Synthetic Event-Based Vision Datasets for Navigation and Landing. In
*Proceedings of the 12th International Conference on Guidance, Navigation & Control Systems
(GNC)*, URL: [https://arxiv.org/abs/2308.00394](https://arxiv.org/abs/2308.00394), 2023

Lately, event-cameras have garnered a lot of interest in the navigation community owing to their high dynamic range, high temporal resolution and lower power usage when compared to standard frame-based cameras. The absence of datasets tailored to navigation applications to date have limited its adoption beyond conceptual levels. The primary objective of this data pipeline is to allow users to generate event-based datasets from (optimal) landing trajectories during the approach of a target body, simulating what an event camera would see in such a context.

Sequences of photorealistic images of the lunar surface are generated with the Planet and Asteroid Natural Scene Generator Utility (PANGU) at different viewpoints along a set of optimal descent trajectories obtained by varying the boundary conditions. The generated image sequences are then converted into event streams by means of an event-based camera emulator. The pipeline then outputs realistic event-based representations of surface features, such as craters and the lunar surface horizon, supporting various spacecraft pose reconstruction problems given events as input.

## Pipeline

![traj2e pipeline](./media/pipeline.png)


![PANGU frame](./media/frame.png)  |  ![Motion field](./media/mf.png) | ![Event-frame](./media/event_frame.png)
:-------------------------:|:-------------------------:|:------------------:
Photorealistic rendering            |  Motion field   | Synthetic events 

## Dependencies

The following dependencies are needed to run the trajectory-to-event pipeline.

### Trajectory optimization

We rely on the AMPL modelling language to express the optimal control problem corresponding to the free and pinpoint lunar landings that make up the dataset (using SNOPT as the backend solver). Licensing information can be found [here](https://ampl.com/start-free-now/).

### Scene generation

The Planet and Asteroid Natural Scene Generation Utility (PANGU) from STAR-Dundee is used to generate photorealistic renders of the lunar surface at different viewpoints along the landing trajectory. Licensing information for ESA projects can be found [here](https://pangu.software/esa-license-request/). Additional information is also available [here](https://www.star-dundee.com/products/pangu-planet-and-asteroid-natural-scene-generation-utility/#product_features).

### Synthetic event generation

The generation of synthetic events from the videos of lunar landing simulations is based on the video-to-event utility part of PROPHESEE's [Metavision Software Toolkit](https://www.prophesee.ai/metavision-intelligence/). Instructions detailing the installation of the open-source `OpenEB` library (sufficient for this project) and the optional, free Metavision SDK can be found [here](https://docs.prophesee.ai/stable/installation/index.html). 

## Installation

We recommend setting up a virtual environment to keep track of all the Python dependencies. Install and activate the virtual environment by running
```bash
virtualenv -p python3.9 venv
source venv/bin/activate
```

The remaining Python dependencies can be installed by running
```bash
pip install -e .
```

The following instructions will install `OpenEB` locally (taken directly from the project's [README](https://github.com/prophesee-ai/openeb/blob/main/README.md)).

### OpenEB prerequisites

Install the following dependencies:

```bash
sudo apt update
sudo apt install apt-utils build-essential software-properties-common wget unzip curl git cmake
sudo apt install libopencv-dev libboost-all-dev libusb-1.0-0-dev
sudo apt install libhdf5-dev hdf5-tools libglew-dev libglfw3-dev libcanberra-gtk-module ffmpeg 
```


The Python bindings of the C++ API rely on the [pybind11](https://github.com/pybind) library, specifically version 2.6.0. Unfortunately, there is no pre-compiled version of pybind11 available, so you need to install it manually:

```bash
wget https://github.com/pybind/pybind11/archive/v2.6.0.zip
unzip v2.6.0.zip
cd pybind11-2.6.0/
mkdir build && cd build
cmake .. -DPYBIND11_TEST=OFF
cmake --build .
sudo cmake --build . --target install
cd ../../
```

### OpenEB compilation

 1. Retrieve the `OpenEB` source code:
    ```bash
    git clone https://github.com/prophesee-ai/openeb.git --branch 4.2.1
    ```
 2. Create and open the build directory in the `openeb` folder (absolute path to this directory is called `OPENEB_SRC_DIR` in next sections)
    ```bash
    mkdir build && cd build
    ```
 3. Generate the makefiles using CMake. If you want to specify to cmake which version of Python to consider, you should use the option `-DPython3_EXECUTABLE`:
    ```bash
    cmake .. -DBUILD_TESTING=OFF -DPython3_EXECUTABLE=/usr/bin/python3.9 -DPYTHON3_SITE_PACKAGES=<path-to>/trajectory-to-events/venv/lib/python3.9/site-packages/
    ```
 4. Compile:
    ```bash
    cmake --build . --config Release -- -j 4
    ```
 5. Deploy `OpenEB` in the system path
    ```bash
    sudo cmake --build . --target install
    ```
 6. Update `LD_LIBRARY_PATH` and `HDF5_PLUGIN_PATH` (which you may add to your `~/.bashrc` to make it permanent):
    ```bash
    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib
    export HDF5_PLUGIN_PATH=$HDF5_PLUGIN_PATH:/usr/local/hdf5/lib/plugin
    ```

    Note that you ou can also deploy the OpenEB files (applications, samples, libraries etc.) in a directory of your choice by using
    the `CMAKE_INSTALL_PREFIX` variable (`-DCMAKE_INSTALL_PREFIX=<OPENEB_INSTALL_DIR>`) when generating the makefiles
    in step 3. Similarly, you can configure the directory where the Python packages will be deployed using the
    `PYTHON3_SITE_PACKAGES` variable (`-DPYTHON3_SITE_PACKAGES=<PYTHON3_PACKAGES_INSTALL_DIR>`).


## Citing

```
@inproceedings{azzalini2023,
  title={On the Generation of Synthetic Event-Based Vision Datasets for Navigation and Landing},
  author={Loïc J. Azzalini and Emmanuel Blazquez and Alexander Hadjiivanov and Gabriele Meoni and Dario Izzo},
  booktitle={Proceedings of the 12th International Conference on Guidance, Navigation & Control Systems(GNC)},
  address={Sopot, Poland},
  year={2023}
}
```